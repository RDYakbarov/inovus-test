import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { fakeBackendProvider } from './helpers/index';

import { AppComponent } from './app.component';
import { AlertComponent } from './directives/index';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import { EntryComponent } from './entry/entry.component';
import { HeaderComponent } from './components/header-part/header/header.component';
import { FooterComponent } from './components/footer-part/footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { AlertService, AuthenticationService, UserService } from './services/index';
import { DashboardService } from './services/dashboard.service';

import { AuthGuard } from './guards/index';
import { JwtInterceptor } from './helpers/index';

import { routing } from './routes';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    routing,
    NgbModule.forRoot()
  ],
  declarations: [
    AppComponent,
    AlertComponent,
    LoginComponent,
    RegisterComponent,
    EntryComponent,
    HeaderComponent,
    FooterComponent,
    DashboardComponent
  ],
  providers: [
    AuthGuard,
    AlertService,
    AuthenticationService,
    DashboardService,
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    fakeBackendProvider
  ],
  bootstrap: [
    AppComponent
  ]
})

export class AppModule { }
