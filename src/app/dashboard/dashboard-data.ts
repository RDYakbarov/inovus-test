
import { Dashboard } from '../models/dashboard';

export const DASHBOARD_ITEMS: Dashboard[] = [{
  id: 1,
  name: 'Анис',
  description: 'Плоды мелкие, средний вес 63 грамма, плоскоокруглой, репчатой формы,' +
  'крона широкопирамидальная, с возрастом округлая, густоты средней.',
  season: 'лето'
}, {
  id: 2,
  name: 'Апорт',
  description: 'Кожица апорта плотная, жёлтая или жёлто-зелёная с красно-коричневой росписью; мякоть отличается рассыпчатостью и нежным вкусом.',
  season: 'осень'
}, {
  id: 3,
  name: 'Антоновка',
  description: 'Плоды среднего размера, 120—150 г, максимум 300 г. Форма изменчива от плоско-округлой до овально-конической, иногда цилиндрическая, ' +
  'поверхность гранёная или широко ребристая.',
  season: 'осень'
}];
