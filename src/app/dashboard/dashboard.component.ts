import { Component, OnInit } from '@angular/core';


import { clone } from 'lodash';
import { Dashboard } from '../models/dashboard';
import { DashboardService } from '../services/dashboard.service';

@Component({
  moduleId: module.id,
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {

  appName = "Список сортов яблок";

  dashboard: Dashboard[];
  dashboardForm: boolean = false;
  editDashboardForm: boolean = false;
  isNewForm: boolean;
  newDashboard: any = {};
  editedDashboard: any = {};

  constructor(private _dashboardService: DashboardService) { }

  ngOnInit() {
    this.getDashboard();
  }

  getDashboard() {
    this.dashboard = this._dashboardService.getDashboardFromData();
  }

  showEditDashboardForm(dashboard: Dashboard) {
    if(!dashboard) {
      this.dashboardForm = false;
      return;
    }
    this.editDashboardForm = true;
    this.editedDashboard = clone(dashboard);
  }

  showAddDashboardForm() {
    // resets form if edited dashboard
    if(this.dashboard.length) {
      this.newDashboard = {};
    }
    this.dashboardForm = true;
    this.isNewForm = true;
  }

  saveDashboard(dashboard: Dashboard) {
    if(this.isNewForm) {
      // add a new dashboard
      this._dashboardService.addDashboard(dashboard);
    }
    this.dashboardForm = false;
  }

  removeDashboard(dashboard: Dashboard) {
    this._dashboardService.deleteDashboard(dashboard);
  }

  updateDashboard() {
    this._dashboardService.updateDashboard(this.editedDashboard);
    this.editDashboardForm = false;
    this.editedDashboard = {};
  }

  cancelNewDashboard() {
    this.newDashboard = {};
    this.dashboardForm = false;
  }

  cancelEdits() {
    this.editedDashboard = {};
    this.editDashboardForm = false;
  }

}
