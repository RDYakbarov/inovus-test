import { Injectable } from '@angular/core';
import { Dashboard } from '../models/dashboard';
import { findIndex } from 'lodash';
import { DASHBOARD_ITEMS } from '../dashboard/dashboard-data';


@Injectable()
export class DashboardService {
  private dItems = DASHBOARD_ITEMS;

  getDashboardFromData(): Dashboard[] {
    console.log(this.dItems);
    return this.dItems
  }

  addDashboard(dashboard: Dashboard) {
    this.dItems.push(dashboard);
    console.log(this.dItems);
  }

  updateDashboard(dashboard: Dashboard) {
    let index = findIndex(this.dItems, (d: Dashboard) => {
      return d.id === dashboard.id;
    });
    this.dItems[index] = dashboard;
  }

  deleteDashboard(dashboard: Dashboard) {
    this.dItems.splice(this.dItems.indexOf(dashboard));
    console.log(this.dItems);
  }

}
