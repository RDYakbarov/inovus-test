export interface Dashboard {
  id: number;
  name: string;
  description: string;
  season: any;
}
